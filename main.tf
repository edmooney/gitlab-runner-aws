#Declare provider
provider "aws" {
  access_key      = lookup(var.aws_access_keys, "access_key")
  secret_key      = lookup(var.aws_access_keys, "secret_key")
  region          = lookup(var.aws_access_keys, "region")
}

#Import the security group module
module "runner-sg" {
  source        = "./modules/sg/"
  sg_name       = "runner-sg"
}

#Import the elb module
module "runner-elb" {
  source        = "./modules/elb/"
  elb_name = "runner-elb"
}

#Create launch configuration for autoscaling group
resource "aws_launch_configuration" "runner-launch-config" {
  name_prefix   = "rancher-launch-config"
  image_id      = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  user_data = "${base64encode(file("${var.user_data_file}"))}"
  key_name = "${var.key_name}"
  security_groups = ["${module.runner-sg.sg-id}"]
}

#Create autoscaling group
resource "aws_autoscaling_group" "runner-asg" {
  name                      = "runner-asg"
  max_size                  = 1
  min_size                  = 1
  health_check_grace_period = 100
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.runner-launch-config.name}"
  vpc_zone_identifier       = ["${lookup(var.subnets, "subnet01")}", "${lookup(var.subnets, "subnet02")}"]

  initial_lifecycle_hook {
    name                 = "lifecycle-continue"
    default_result       = "CONTINUE"
    heartbeat_timeout    = 30
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  }
}

#Attach autoscaling group to elb
resource "aws_autoscaling_attachment" "runner-ha-elb-attach" {
  autoscaling_group_name = "${aws_autoscaling_group.runner-asg.id}"
  elb = "${module.runner-elb.elb-name}"
}

#output "instance_private_ip_addresses" {
#  value = {
#    for instance in aws_autoscaling_group.rancher-ha:
#    instance.id => instance.ip_address
#  }
#}
